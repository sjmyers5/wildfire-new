﻿using UnityEngine;
using System.Collections;

public class BasicWolf : MonoBehaviour {

	public Transform player;

	public bool ableToAttack;

	public float distanceToPlayer;

	public float huntPlayerDistance = 100;

	public bool huntFailure;

	public Vector3 runAwaySpot;

	public bool iKnowWhereToRun;

	public Animator animMaster;

	public Transform animMesh;

	public Quaternion wolfYRot;


	// Use this for initialization
	void Start ()
	{
	



	}
	
	// Update is called once per frame
	void Update ()
	{
	
		if(huntFailure)
		{

			CalculateEscapeRoute();

			RetreatHere();

			return;

		}

		//distanceToPlayer = Mathf.Abs((player.position - transform.position).sqrMagnitude);

		//Debug.Log(distanceToPlayer);

		//checkDistanceToPlayer();


		if(ableToAttack == true)
		{

			transform.position = Vector3.Lerp(transform.position, player.position, (Time.deltaTime * 3));

			transform.LookAt(player);

			wolfYRot = player.rotation;

			wolfYRot.x = 0;

			wolfYRot.z = 0;

			transform.rotation = wolfYRot;

		}


	}

	void checkDistanceToPlayer ()
	{

		if(distanceToPlayer <= huntPlayerDistance)
		{

			ableToAttack = true;

		}

	}

	void GoAway ()
	{

		ableToAttack = false;

		huntFailure = true;

		animMaster.SetBool("isAttacking", false);

	}

	void CalculateEscapeRoute ()
	{

		if(iKnowWhereToRun == false)
		{
			
			runAwaySpot = transform.position;

			runAwaySpot.x += Random.Range(-100, 100);

			runAwaySpot.z += Random.Range(-100, 100);

			iKnowWhereToRun = true;

		}

	}


	void RetreatHere ()
	{

		transform.position = Vector3.Lerp(transform.position, runAwaySpot, (Time.deltaTime * .4f));
		transform.LookAt(runAwaySpot);
		animMaster.SetBool("isMoving", true);

	}

	void killThisPerson (Transform villager)
	{

		player = villager;

		ableToAttack = true;

		animMaster.SetBool("isMoving", true);

	}

	void attackAnimation ()
	{

		this.transform.position = animMesh.position;

		this.transform.rotation = animMesh.rotation;

		animMaster.SetBool("isAttacking", true);

	}


}
