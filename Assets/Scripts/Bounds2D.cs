﻿using UnityEngine;
using System.Collections;

public class Bounds2D : MonoBehaviour {

	public Vector2 center = new Vector2(0,0);
	public float x; // Half width
	public float y; // Half height

	// The unit square
	public Bounds2D()
	{
		this.center = Vector2.zero;
		this.x = .5f;
		this.y = .5f;
	}

	public Bounds2D(Vector2 center, float x, float y)
	{
		this.center = center;
		this.x = x;
		this.y = y;
	}


	public Bounds2D(Vector2 posCorner, Vector2 negCorner)
	{
		this.center = (posCorner + negCorner);
		this.x =  posCorner.x - this.center.x;
		this.y = posCorner.y - this.center.y;
	}
}
