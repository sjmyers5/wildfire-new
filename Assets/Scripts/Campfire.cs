﻿using UnityEngine;
using System.Collections.Generic;

// Campfires will the the base for a number of villagers
public class Campfire : MonoBehaviour {
	 
	public Villager[] villagerPrefabs;
	public List<Villager> villagers;
	public Vector3 villagerSpawn;

	public Vector3 scaleVect;

	public int fuelInit = 0;	// The fuel that a new fire starts with
	public int fuelGrow = 9;	// The fuel level that causes the fire to grow

	// Private members
	public int fuelCurr;	// The current fuel of the fire, private, only changed through methods
	public bool spreadable = false;

	public void Start(){
		villagerSpawn = Vector3.zero;

		//scaleVect = new Vector3(10f, 10f, 10f);

		fuelCurr = fuelInit;
		this.fuelInit = 0;
		this.fuelGrow = 9;

		SpawnVillagers(1);
	}

	public void OnDestroy(){
		// Tell all its villagers that it was destroyed
		foreach(Villager villager in villagers)
			villager.CampDestroyed();
	}

	// Add fuel to the fire, fire grows when fuelCurr >= reaches fuelGrow
	public void AddFuel(int addedFuel){
		if (!spreadable && (fuelCurr += addedFuel) >= fuelGrow){
			fuelCurr = fuelGrow; // Set the fuel
			gameObject.transform.localScale += scaleVect; // Scale the fire
			spreadable = true; // Mark it as spreadable
		}
	}

	public bool TakeSubFire(){
		if(!spreadable) return false;
		fuelCurr = fuelInit;
		transform.localScale -= scaleVect;
		spreadable = false;
		return true;
	}

	// Snuff out this campfire, for now just destroys the gameobject
	public void SnuffOut(){
		Destroy(this.gameObject);
	}

	public void SpawnVillagers(int amount){
		Vector3 offset;
		float minDist = .5f, maxDist = 3f;
		RaycastHit hit;
		for (int i = 0; i < amount; i++){
			offset = Random.insideUnitCircle.normalized * Random.Range(minDist, maxDist);
			World.GetGroundPos(villagerSpawn + offset, out hit);
			this.AddVillager( (Villager) Instantiate(villagerPrefabs[Random.Range(0, villagerPrefabs.Length)],
			                                         hit.point, Quaternion.identity));
		}
	}

	private bool AddVillager(Villager villager){
		villager.camp = this;
		villagers.Add(villager);
		return true;
	}
}
