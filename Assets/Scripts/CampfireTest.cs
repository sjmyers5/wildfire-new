﻿using UnityEngine;
using System.Collections.Generic;

// Campfires will the the base for a number of villagers
public class CampfireTest : MonoBehaviour {
	 
	public VillagerTest[] villagerPrefabs;
	public List<VillagerTest> villagers;
	//public Vector3 villagerSpawn;

	public GameObject cave;

	public GameObject popUp;

	public Vector3 scaleVect;

	public int fuelInit = 0;	// The fuel that a new fire starts with
	public int fuelGrow = 1;	// The fuel level that causes the fire to grow

	// Private members
	public int fuelCurr;	// The current fuel of the fire, private, only changed through methods
	public bool spreadable = false;

	public void Start(){
		//villagerSpawn = Vector3.zero;
		//scaleVect = new Vector3(10f, 10f, 10f);
		if(cave == null)
		{
			cave = GameObject.FindGameObjectWithTag("Cave");

		}

		cave.SendMessage("GiveMeAPerson", this.gameObject);

		fuelCurr = fuelInit;
		this.fuelInit = 0;
		this.fuelGrow = 1;

		//SpawnVillagers(1);
	}

	public void ThisPerson(GameObject villy)
	{
		villagers[0] = villy.GetComponent<VillagerTest>();


	}

	void turnOnPopup()
	{
		if(spreadable){
			popUp.SetActive(true);

		}
	}

	void turnOffPopup()
	{
		
		popUp.SetActive(false);


	}


	/*public void OnDestroy(){
		// Tell all its villagers that it was destroyed
		foreach(VillagerTest villager in villagers)
			villager.CampDestroyed();
	}*/

	// Add fuel to the fire, fire grows when fuelCurr >= reaches fuelGrow
	public void AddFuel(int addedFuel){
		if (!spreadable && (fuelCurr += addedFuel) >= fuelGrow){
			fuelCurr = fuelGrow; // Set the fuel
			this.transform.localScale += scaleVect * 3; // Scale the fire
			spreadable = true; // Mark it as spreadable
		}
	}

	public bool TakeSubFire(){
		if(!spreadable) return false;
		fuelCurr = fuelInit;
		transform.localScale -= scaleVect;
		spreadable = false;
		return true;
	}

	// Snuff out this campfire, for now just destroys the gameobject
	public void SnuffOut(){
		cave.SendMessage("GetAnotherFire", villagers[0].gameObject);
		this.gameObject.SetActive(false);
		//Destroy(this.gameObject);
	}

	public void FindReplacement()
	{
		cave.SendMessage("GiveMeAPerson", this.gameObject);
	}

	/*public void SpawnVillagers(int amount){
		Vector3 offset;
		float minDist = .5f, maxDist = 3f;
		RaycastHit hit;
		for (int i = 0; i < amount; i++){
			offset = Random.insideUnitCircle.normalized * Random.Range(minDist, maxDist);
			World.GetGroundPos(villagerSpawn + offset, out hit);
			this.AddVillager( (VillagerTest) Instantiate(villagerPrefabs[Random.Range(0, villagerPrefabs.Length)],
			                                         hit.point, Quaternion.identity));
		}
	}*/

	/*public bool AddVillager(VillagerTest villager){
		//villager.camp = this;
		villagers.Add(villager);
		return true;
	}*/
}
