using UnityEngine;
using System.Collections.Generic;

public class Forest : MonoBehaviour {
	
	public MyTree[] treePrefabs;

	public int spacing = 5;
  public int maxTrees = 500;
	public int numSeeds = 5;
	public int maxSpacing = 20;
  public Vector2 forestOrigin = new Vector2(0.0f,0.0f);
	public int width;
	public int depth;
	
	// Use this for initialization
	public Forest(){}

	// When placing trees, make sure they are within forestBounds
	public void Generate(){
		/*float halfWidth = width/2;
		float halfDepth = depth/2;
		for (int x = (int) - Mathf.Ceil(halfWidth); x < (int) Mathf.Ceil(halfWidth); ++x) {
			for (int y = (int) - Mathf.Ceil(halfDepth); y < (int) Mathf.Ceil(halfDepth); ++y) {
				float angle = Random.Range(0.0f, 6.28f);
				float radius = Random.Range(0.0f, (float)spacing / 4);
				EmplaceTree (Vector2.zero + new Vector2(spacing * x, spacing * y)
				             + new Vector2( radius * Mathf.Cos(angle), radius*Mathf.Sin(angle) )
				             - new Vector2( (float) width * spacing / 4,(float) y * spacing / 4) ); 
			}
		}*/
    int nTrees = 0;
    Queue<Vector2> posQueue = new Queue<Vector2>();
    posQueue.Enqueue(this.forestOrigin);
		while(nTrees < maxTrees && posQueue.Count > 0) {
      Vector2 candidatePos = posQueue.Dequeue();
      if(this.Accept(candidatePos)) {
        EmplaceTree(candidatePos);
        nTrees++;
				for( int i = 0; i < numSeeds; ++i) {
					float angle = Random.Range(0.0f, 6.28f);
					float radius = Random.Range(0.0f, maxSpacing);
					posQueue.Enqueue(candidatePos + new Vector2( radius * Mathf.Cos(angle), radius*Mathf.Sin(angle) ) );
				}
      }
      
    }
	}

	private bool Accept(Vector2 pos) {
		RaycastHit hit;
		if(!World.GetGroundPos(pos, out hit) ) return false;

		foreach(GameObject tree in GameObject.FindGameObjectsWithTag("Tree")) {
			if((hit.point - tree.transform.position).magnitude < spacing) return false;	
		}

		return true;
	}

	private void EmplaceTree (Vector2 pos){
		RaycastHit hit;
		if( World.GetGroundPos(pos, out hit)){
			Instantiate (treePrefabs[Random.Range(0, treePrefabs.Length)], hit.point, Quaternion.identity);
			Debug.Log("Trying to place tree at " + hit.point);

		}
			
	}


}
