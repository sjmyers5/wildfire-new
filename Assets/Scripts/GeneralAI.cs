﻿using UnityEngine;
using System.Collections;

// [RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(Animator))]
abstract public class GeneralAI : AIPath{

	// World is the way to access all the lists of entities (trees, villagers, wolves, campfires)


	//S changed this from protected to public
	public Animator anim; // Holds the animation states

	//S changed this from protected to public
	protected override void Awake () {
		base.Awake(); // For now just use AIPath.Awake()
		anim = GetComponent<Animator>();
	}

	// Must override Update, this is the AI's main thinking method
	abstract override public void Update  ();

	public void MoveTowardTarget()
	{
		Vector3 dir = CalculateVelocity (GetFeetPosition());
		
		//Rotate towards targetDirection (filled in by CalculateVelocity)
		RotateTowards (targetDirection);

		// TODO try to use character controller
		if (controller != null)
			controller.SimpleMove (dir);
		if (controller == null)
		{
			Debug.Log("Null Controller");
			tr.Translate (dir*Time.deltaTime, Space.World);
		}
	} 

}