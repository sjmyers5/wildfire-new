﻿using UnityEngine;
using System.Collections;

public class MyTree : MonoBehaviour {
	
	//public Transform realTree;
	public bool available = true;
	public Mesh stump;

	public bool hasBeenChopped;

	public Animator anim;

	public int fuelValue;


	public Material leavesMaterial;

	public Material underLeavesMaterial;

	public Material trunkMaterial;

	public float blendedMatAmount;

	public float timeLerpingMats = 1f;

	public float lerp;

	public Color colorAlpha;

	public bool isLerping;

	public SkinnedMeshRenderer leavesMesh;

	public SkinnedMeshRenderer underLeavesMesh;

	public SkinnedMeshRenderer trunkMesh;


	void Awake(){
		this.gameObject.tag = "Tree";
		this.gameObject.layer = 8;
		this.fuelValue = 3;
	}

	void Start () {

	
		leavesMaterial = leavesMesh.material;

		trunkMaterial = trunkMesh.material;

		underLeavesMaterial = underLeavesMesh.material;

		colorAlpha.r = 1;
		colorAlpha.g = 1;
		colorAlpha.b = 1;

		lerp = 1;



	}

	void Update ()
	{

		if(hasBeenChopped)
		{

			//lerpNow();

			StartCoroutine(waitThenFade());


		}




	}



	// Clean up when the tree is destroyed
	void OnDestroy() {

	}

	public int Fell(){




		hasBeenChopped = true;

		//isLerping = true;

		anim.SetBool("Timber", true);

		leavesMesh.SendMessage("fadeNow");

		available = false;

		return fuelValue;

		hasBeenChopped = true;

		/*if(isLerping)
		{



			lerp = Mathf.Lerp(lerp, 0, Time.deltaTime);

			colorAlpha.a = lerp;

			treeMaterial.SetColor("_Color", colorAlpha);

		}*/
	
		//Destroy (this.gameObject);

	}


	void lerpNow()
	{

		//StartCoroutine(waitThenFade());

		lerp = Mathf.Lerp(lerp, 0, Time.deltaTime);

		colorAlpha.a = lerp;

		leavesMaterial.SetColor("_Color", colorAlpha);

		underLeavesMaterial.SetColor("_Color", colorAlpha);

		trunkMaterial.SetColor("_Color", colorAlpha);



	}

	IEnumerator waitThenFade()
	{


		yield return new WaitForSeconds(1);

		lerpNow();

	


	}


}
