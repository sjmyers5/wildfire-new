﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class OldWorld : MonoBehaviour
{

	private Collider terrain;

	public GameObject tree;

	public Vector2 forestOrigin;
	public int forestDepth = 10;
	public int forestWidth = 10;
	public float spacing = 20;

	//private AstarPath astar;
	private GridGraph gg;

	void Awake ()
	{
		terrain = GameObject.FindGameObjectWithTag ("Ground").GetComponent<MeshCollider> ();

		print("Test");

		GenerateForest ();
		
		
		AstarData data = AstarPath.active.astarData;
		
		//creates a Grid Graph
		GridGraph gg = data.AddGraph (typeof(GridGraph)) as GridGraph;
		
		// Setup a grid graph with some values
		gg.width = 50;
		gg.depth = 50;
		gg.nodeSize = 1;
		gg.center = Vector3.zero;
		
		// Updates internal size from the above values
		gg.UpdateSizeFromWidthDepth ();
		
		// Scans the terrain to create the graph
		AstarPath.active.Scan ();
	}

	// Use this for initialization
	void Start ()
	{
	}

	private void GenerateForest ()
	{
		for (int i = 0; i < forestWidth; ++i) {
			for (int j = 0; j < forestDepth; ++j) {
				//int numTrees = (int)(Mathf.PerlinNoise(
        float angle = Random.Range(0.0f, 6.28f);
        float radius = Random.Range(0.0f, (float)spacing/2);
				EmplaceTree (forestOrigin + new Vector2 (spacing * i, spacing * j) + new Vector2(radius*Mathf.Cos(angle),radius*Mathf.Sin(angle)) - new Vector2((float)forestWidth*spacing/2,(float)forestDepth*spacing/2));
			}
		}
	}


	private void EmplaceTree (Vector2 pos2D)
	{
		RaycastHit hitInfo;
		if (terrain.Raycast (new Ray (new Vector3 (pos2D.x, 1000, pos2D.y), -transform.up), out hitInfo, Mathf.Infinity))
			Instantiate (tree, hitInfo.point, Quaternion.identity);
	}


}
