﻿using UnityEngine;
using System.Collections.Generic;

public class Villager : GeneralAI
{

	public enum State {
		// Tree
		StartTree,
		ToTree,
		AtTree,
		NoTree,

		// Camp
		StartCamp,
		ToCamp,
		AtCamp,
		NoCamp
	};
	
	public State state; // The current state of the villager

	public Campfire camp; // Reference to home camp

	public MyTree targetTree; // Referene to target tree

	public float treeStandDist = 3f; // Distance to stand from the tree while chopping

	private float timer = 0f;
	public float waitTime = 3f;

	public int carriedFuel = 0;
	
	public bool huntable = false;
	
	private Transform persTransform;

	public Transform model;

	public GameObject scoreMaster;

	public Animator anim;

	public bool chopped;

	protected override void Start ()
	{
		// Some of the setup
		base.Start();

		persTransform = new GameObject().transform;
		state = State.StartCamp;
	}

	void OnDestroy ()
	{

	}

	/*
	 * Should work for now, Update() is not called concurrently so it doesn't need locks
	 */
	public MyTree GetNearestTree ()
	{
		MyTree currTree = null, nearTree = null;
		float currDist, nearDist = Mathf.Infinity;

		foreach (GameObject o in GameObject.FindGameObjectsWithTag("Tree")) {
			currTree = (MyTree) o.gameObject.GetComponent<MyTree> (); // We know it will have a Tree script
			currDist = Vector3.Distance(this.transform.position, o.transform.position);
			if (currTree.available && currDist < nearDist) {
				nearDist = currDist;
				nearTree = currTree;
			}
		}

		if (nearTree != null)
			nearTree.available = false; // Tell other villagers not to take that tree

		return nearTree;
	}

	public override void OnTargetReached ()
	{
		switch (state) {
		case State.ToTree:
			target = targetTree.transform;
			state = State.AtTree;
			break;
		case State.ToCamp:
			huntable = true;
			state = State.AtCamp;
			break;
		default:
			break;
		}
	}

	/*void OnTriggerEnter (Collider col)
	{

		if (col.tag == "Wolf") {
			anim.SetBool ("isAttacked", true);
			//animMaster.SetBool("isDead", true);
			GameObject.FindGameObjectWithTag ("Score").SendMessage ("villagerDeath");
			Destroy (this.gameObject); // Remove the Villager
		}

	}*/

	/*
	 * When the player snuffs out this villager's campfire, what does he do?
	 */
	public void CampDestroyed (){

		state = State.NoCamp;
	}

	// TODO something to check if they can't reach their destination
	override public void Update ()
	{
		switch (state) {
		case State.StartTree: // Initialize the ToTree state
			targetTree = GetNearestTree ();
			if(targetTree != null){
				anim.SetBool ("isMoving", true);
				base.endReachedDistance = 0.05f;
				persTransform.position = targetTree.transform.position + (targetTree.transform.forward.normalized * treeStandDist);
				target = persTransform;
				state = State.ToTree;
			}
			else
				state = State.NoTree;
			break;

		case State.ToTree:
			base.MoveTowardTarget ();
			break;

		case State.AtTree:
			// TODO Make sure Villager is turned toward the tree.
			anim.SetBool("isMoving", false);//stop walking in place when you get to the tree, silly villagers!
			timer += Time.deltaTime;
			if (timer <= waitTime) break;
			timer = 0f;

			Harvest( targetTree.Fell() ); // Fell the tree and Harvest the fuel
			state = State.StartCamp;
			break;

		case State.NoTree:
			Destroy (this.gameObject); // TODO wut do if no trees?
			break;

		case State.StartCamp:
			if(camp != null){
				anim.SetBool ("isMoving", true);
				target = camp.transform;
				base.endReachedDistance = 0.5f;
				state = State.ToCamp;
			}
			else
				state = State.NoCamp;
			break;

		case State.ToCamp:
			MoveTowardTarget ();
			break;

		case State.AtCamp:
			timer += Time.deltaTime;
			if (timer <= waitTime) break;
			timer = 0f;

			TendCamp ();
			state = State.StartTree;
			break;

		case State.NoCamp:
			Destroy (this.gameObject);
			break;

		default:
			break;
		}
	}

	private void Harvest(int fuel)
	{
		this.carriedFuel += fuel; // For now just a simple addition 
	}

	private void TendCamp()
	{
		camp.AddFuel(carriedFuel); // Could make it only add as much as needed.
		carriedFuel = 0;
	}

	public void WolfHit (GameObject wolfObject)
	{
		wolfObject.SendMessage("GoAway");
		GameObject.FindGameObjectWithTag ("Score").SendMessage ("villagerDeath");
		Destroy (this.gameObject); // Remove the Villager
		anim.SetBool ("isAttacked", true);
		//animMaster.SetBool("isDead", true);
	}

}
