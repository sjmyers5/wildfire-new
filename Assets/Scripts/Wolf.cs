﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Wolf : GeneralAI
{

	enum State {Attack, Prowl, Retreat, Struggle};
	public int StateIterations = 4;
	public float StateSpread = 10;
 	public float CampfireRepellance = 1.0f;
	public float WolfRepellance = 1.0f;
	public float VillagerAttraction = 0.95f;
	public float AttackRadius = 100.0f;
	public float SprintSpeed =5.0f;
	public float SprintTurnSpeed = 3f;
	public float SaunterSpeed = 2f;
	public float SaunterTurnSpeed = 1.5f;

	private MeshCollider Terrain;
	GameObject targetObject;
	State currState;
	bool recalcTarget;

	public bool canAttack;

	public float villagerDeathTimer;

	float closeEnoughLoc;

	float villLoc;

	float currentLocation;

	float distanceFromTarget;

	//DO NOT EVER TOUCH THIS VARIABLE OR ANYTHING TO DO WITH IT
	public Animator anim2;

	public Rigidbody wolfRby;
	public bool moveLocked;

	//SOUNDS
	public AudioClip growl;
	public AudioClip attack1;
	public AudioClip footSteps;
	public AudioClip snarling;
	public AudioSource wolfSource;
	public AudioSource snarlSource;
	public AudioSource FootstepsSource;
	public float pitchLow = .75f;
	public float pitchHigh = 1.2f;

	public bool alreadySounded;
	public bool isRetreatCountdown;

	public float CooldownTime = 15f;
	float CooldownCounter = 0f;
	public bool GotAProwlPlace;

	public float distancetoFires;
	public bool iOnlyRunOnce;

	//public Vector3 dir = targetPosition-currentPosition;
	//dir.y = 0;
	//public float targetDist = dir.magnitude;

	// Use this for initialization
	protected override void Start ()
	{



        //StateBranchFactor = 6;
        //StateSpread = 5.0f;
        //MaxNodes = 10;
        //CampfireRepellance = 1.0f;
        //VillagerAttraction = 0.3f;
		Terrain = World.i.GetComponent<MeshCollider> ();
		targetObject = new GameObject ();
		Transform target = targetObject.transform;

		if(anim2 == null)
		{
			anim2 = this.GetComponent<Animator>();
		}

		if(anim == null)
		{
			anim = this.GetComponent<Animator>();
		}

	

		//wolfSource = GetComponent<AudioSource>();
		//snarlSource = GetComponent<AudioSource>();
		//FootstepsSource = GetComponent<AudioSource>();

		this.target = GameObject.FindGameObjectsWithTag("Getaway")[Random.Range(0, 5)].transform;
		GotAProwlPlace = true;

		this.InitState(State.Prowl);

		base.Start ();
	}

	// Update is called once per frame
	override public void Update ()
	{

		if(GameObject.FindGameObjectsWithTag("Campfire") != null)
		{
			foreach(GameObject fireSpot in GameObject.FindGameObjectsWithTag("Campfire"))
			{
				if((this.transform.position - fireSpot.transform.position).sqrMagnitude < 450 && iOnlyRunOnce == false)
				{

					GoAway();
					isRetreatCountdown = true;
					villagerDeathTimer = 5;
					AttackRadius = 4;
					iOnlyRunOnce = true;
				}
			}
		}

		canIMove();

		if(isRetreatCountdown)
		{
			villagerDeathTimer += Time.deltaTime;
		
			if(villagerDeathTimer >= 14)
			{
				villagerDeathTimer = 0;
				isRetreatCountdown = false;
				this.speed = this.SaunterSpeed;
				this.turningSpeed = this.SaunterTurnSpeed;
				AttackRadius = 20;
				iOnlyRunOnce = false;
				anim2.SetBool("isRunning", false);

			}
		
		}

		/*if(GotAProwlPlace)
		{
			ProwlTimer += Time.deltaTime;
		
			if(ProwlTimer >= 15)
			{
				this.target = GameObject.FindGameObjectsWithTag("Getaway")[Random.Range(0, 5)].transform;
				ProwlTimer = 0;
			}
		
		}*/
	

		Debug.Log("Wolf state: " + this.currState);

		switch(this.currState) {

			case State.Prowl:
				wolfGrowlAudio();
				//anim.SetBool("isProwling", true);
				//assess whether to attack
				this.CooldownCounter -= Time.deltaTime;
				Transform putativeTarget = (this.CooldownCounter < 0f) ? FindTarget() : null;
				//Debug.Log("Wolf state: " + this.currState);
				if (putativeTarget && !isRetreatCountdown)
					{
						this.target = putativeTarget;
						this.InitState(State.Attack);
						wolfGrowlAudio();

					} else {

						if(recalcTarget)
						{
      	    				this.target.transform.position = this.StateSearch(this.transform.position);
							recalcTarget = false;
						}

      	  		MoveTowardTarget();
   				    }
				break;

			case State.Attack:

				wolfRunningAudio();	

				//WhereAreWe();

			  //Maybe retreat counter is getting set to zero repeatedly?
			  if(isRetreatCountdown == false){
			  	villagerDeathTimer = 0;
			  }
			  	anim2.SetBool("isRunning", true);

			  	//WhereAreWe();

			  if(CloseEnoughToTarget())
			  {
 	      	  	 	
			  	Debug.Log("WE GOT EM, COACH!!!!");

			  	//wolfAttack1Audio();

			  	transform.position = Vector3.Lerp(transform.position, target.position, (Time.deltaTime * 3));

			  	transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, (Time.deltaTime * 3));

			  	if((transform.position - target.position).sqrMagnitude < 0.5f)
			  	{
			  		StartStruggle();

			  	}


			  	//this.InitState(State.Struggle);

			  		//this.GoAway();
			  }
			  MoveTowardTarget ();

			  break;

			case State.Struggle:
				
				Debug.Log("Wolf movement locked");

				moveLocked = true;
				

				snarlAudio();
				//snarlSource.Play(snarling);



				//transform.LookAt(villLoc);

				//wolfYRot = player.rotation;

				//wolfYRot.x = 0;

				//wolfYRot.z = 0;

				//transform.rotation = wolfYRot;

				anim2.SetBool("isAttacking", true);
				isRetreatCountdown = true;
				//villagerDeathTimer += Time.deltaTime;

				if(villagerDeathTimer >= 4)
				{
						
					moveLocked = false;
					this.InitState(State.Retreat);

				}
				break;

			case State.Retreat:
				
			//wolfSource.Stop(snarling);
				//WhereAreWe();
				//TODO wait for seconds then start looking for villagers again
				canAttack = false;

				//GoAway initializes retreat state, not needed to sustain it
				//GoAway();

				/*
				 * See OnTargetReached()
				if(CloseEnoughToTarget())
				{
	
					Debug.Log("The wolf escaped!");
	
					this.InitState(State.Prowl);
	
					//this.GoAway();
				}*/
				MoveTowardTarget ();
	
					//anim.SetBool("isWalking", true);
				break;
		}
		//this.target.position = this.StateSearch (this.transform.position);
		//Debug.Log(this.target.position);
		//base.target = targetObject.transform;
		//base.Update();
		//MoveTowardTarget ();
	}


	void canIMove ()
	{

		if(moveLocked)
		{
			
			//lock position when in struggle state
			//There is no rigidbody for the wolf?
			//wolfRby.velocity = Vector3.zero;

		}

	}

	/*
	 * This vector math makes no sense. Can report low distances for positions
	 * 	nowhere near each other.
	void WhereAreWe ()
	{
		//figure out our distance from our target and whether we're close enough to call it done

		closeEnoughLoc = 5f;

		villLoc = (target.transform.position).magnitude;

		currentLocation = (this.transform.position).magnitude;

		distanceFromTarget = currentLocation - villLoc;
	}*/

	bool CloseEnoughToTarget() {
		//sqrMagnitude used for efficiency
		return (this.transform.position - this.target.transform.position).sqrMagnitude < 25f;
	}


	//given a transform from a collision, confirm that the wolf is in attack mode
	//and is targeting that specific entity, i.e. "do we attack this thing?"
  public bool VerifyTarget(Transform toVerify)
  {
		return this.currState == State.Attack && toVerify == this.target;

		Debug.Log("Uhhh verified I guess? WTF?");
  }
		

  void StartStruggle () {
		Debug.Log("starting struggle message received!");
		this.InitState(State.Struggle);
  }

	void InitState(State state)
	{
		
		this.currState = state;

		switch(state)
		{

			case State.Prowl:

				if(!isRetreatCountdown){
  			   this.speed = this.SaunterSpeed;;
		       this.turningSpeed = this.SaunterTurnSpeed;
					anim2.SetBool("isRunning", false);
					this.target = this.targetObject.transform;
					this.recalcTarget = true;
				}

				anim2.SetBool("isWalking", true);
				
				break;

			case State.Retreat:
			
				anim2.SetBool("isAttacking", false);
				anim2.SetBool("isRunning", true);
		        this.speed = this.SprintSpeed;
		        this.turningSpeed = this.SprintTurnSpeed;
				this.target = GameObject.FindGameObjectsWithTag("Getaway")[Random.Range(0, 5)].transform;
				//anim2.SetBool("isRunning", false);
	
				Vector3 avoidPoint = new Vector3(0.0f, 0.0f, 0.0f);
				int fireCount = 0;

				foreach(GameObject campfire in GameObject.FindGameObjectsWithTag("Campfire")) {
					avoidPoint += campfire.transform.position;
					fireCount++;
				}
				if (fireCount > 0)
					avoidPoint /= fireCount;
				else
					avoidPoint = this.target.position;

				Vector3 dir = (this.transform.position - avoidPoint).normalized;

				this.targetObject.transform.position = World.i.ProjectToTerrain(this.transform.position + (dir * this.AttackRadius));

				while(CheckIfNotClear()) 
					this.targetObject.transform.position = World.i.ProjectToTerrain(this.targetObject.transform.position + dir * 20.0f);
				//this.target = this.targetObject.transform;

				break;

			case State.Attack:
			
		 		this.repathRate = 0.01f;

				anim2.SetBool("isRunning", true);
				this.speed = this.SprintSpeed;
				this.turningSpeed = this.SprintTurnSpeed;

				break;

			case State.Struggle:
				
				anim2.SetBool("isAttacking", true);
				//set animation stuff

			break;
		}
	}

	public override void OnTargetReached()
	{
		switch (this.currState)
		{
			case State.Prowl:
			//anim2.SetBool("isAttacking", false);
			anim2.SetBool("isRunning", false);
				this.recalcTarget = true;
			break;
 
			case State.Retreat:
				//GoAway initializes retreat state, not needed to sustain it
				if(CheckIfNotClear())
					GoAway();
				else {
					this.InitState(State.Prowl);

				}
				Debug.Log("Okay, mom, I'm going.");
				break;
					/*	default:
								break;*/
		}
	}


	void snarlAudio()
	{

		snarlSource.pitch = Random.Range(pitchLow, pitchHigh);

		snarlSource.PlayOneShot(snarling);


	}


	void wolfGrowlAudio()
	{
		if(alreadySounded == false)
		{
			wolfSource.pitch = Random.Range(pitchLow, pitchHigh);
			wolfSource.clip = growl;
			wolfSource.Play();

			alreadySounded = true;
			Debug.Log("So Many Plays");
		}




	}

	void wolfAttack1Audio()
	{

		wolfSource.pitch = Random.Range(pitchLow, pitchHigh);

		wolfSource.PlayOneShot(attack1);

	}

	void wolfRunningAudio()
	{

		//FootstepsSource.pitch = Random.Range(pitchLow, pitchHigh);

		FootstepsSource.PlayOneShot(footSteps);

	}



	void AttackStart()
	{

		//transform.position = Vector3.Lerp(transform.position, this.position, (Time.deltaTime * 3));


	}

	void GoAway () {
 		//Debug.Log("Initializing Retreat state");

		Debug.Log("Going away now");
		this.InitState(State.Retreat);


	}

	Transform FindTarget ()
	{
	
		Transform result = null;
		float bestDist = Mathf.Infinity;
		float dist;
		foreach(GameObject villager in GameObject.FindGameObjectsWithTag("Villager")) {
			dist = (this.transform.position - villager.transform.position).magnitude;
			if (dist < AttackRadius && dist < bestDist /* TOFIX: add target claiming logic */) {

				anim.SetBool("isRunning", true);
				result = villager.transform;
				bestDist = dist;
				Debug.Log("Found villager in range");
			}
		}

		dist = (this.transform.position - GameObject.FindWithTag("Player").transform.position).magnitude;
		if (dist < AttackRadius && dist < bestDist /* TOFIX: add target claiming logic */) {
			result = GameObject.FindWithTag("Player").transform;
			Debug.Log("Found player in range.");
		}

		return result;
	}

	bool CheckIfNotClear()
	{
		foreach (GameObject villager in GameObject.FindGameObjectsWithTag("Villager"))
			if ((villager.transform.position - this.targetObject.transform.position).magnitude < (this.AttackRadius + 20.0f))
				return true;
		if ((GameObject.FindWithTag("Player").transform.position - this.targetObject.transform.position).magnitude < (this.AttackRadius + 20.0f))
			return true;
		return false;
	}


	Vector3 StateSearch (Vector3 startPos)
	{
		Vector3 bestPos = startPos;

    Vector3 villagerCenter = this.FindCenter("Villager", "Player");
    Vector3 campfireCenter = this.FindCenter("Campfire");
    Vector3 wolfCenter = this.FindCenter("Wolf");

    //Debug.Log("villagerCenter.x: " + villagerCenter.x);
    if(float.IsNaN(campfireCenter.x)) {
      //Debug.Log("NAN DETECTED");
      return bestPos;
    }

		for(int i = 0; i < this.StateIterations; ++i) {
			//some calculus-based black magic
    	//gradient vector gives direction and magnitude of steepest slope at a point
    	Vector3 gradient = new Vector3( CampfireRepellance * (bestPos.x - campfireCenter.x) / (campfireCenter - bestPos).sqrMagnitude - VillagerAttraction * (bestPos.x - villagerCenter.x) / (villagerCenter - bestPos).magnitude + ((GameObject.FindGameObjectsWithTag("Wolf").Length < 2) ? 0 : WolfRepellance * (bestPos.x - wolfCenter.x) / (wolfCenter - bestPos).sqrMagnitude),
      	0.0f,
      	CampfireRepellance * (bestPos.z - campfireCenter.z) / (campfireCenter - bestPos).sqrMagnitude - VillagerAttraction * (bestPos.z - villagerCenter.z) / (villagerCenter - bestPos).magnitude + ((GameObject.FindGameObjectsWithTag("Wolf").Length < 2) ? 0 : WolfRepellance * (bestPos.z - wolfCenter.z) / (wolfCenter - bestPos).sqrMagnitude));

    	//Debug.Log("Gradient vector: " + gradient);
    	//Debug.Log("Campfire sqrMag: " + (campfireCenter - bestPos).sqrMagnitude);
    	//Debug.Log("Villager mag: " + (villagerCenter - bestPos).magnitude);
			bestPos += StateSpread * gradient.normalized;
		}

		return World.i.ProjectToTerrain(bestPos);
	}

  private Vector3 FindCenter (params string[] tags) {
    Vector3 center = new Vector3(0.0f, 0.0f, 0.0f);
    int numObj = 0;
    foreach(string tag in tags)
      foreach(GameObject obj in GameObject.FindGameObjectsWithTag(tag)) {
        center += obj.transform.position;
        numObj++;
      }
    return center / numObj;
  }

	private float EvalUtility (Vector3 pos)
	{
		float utility = 0.0f;
		//farther from fires is better
		foreach (GameObject campfire in GameObject.FindGameObjectsWithTag("Campfire")) {
      //Debug.Log("There are campfires.");
			utility += Mathf.Log ((pos - campfire.transform.position).magnitude, 2.0f) * CampfireRepellance;
		}
		//Debug.Log ("Campfire repellance: " + CampfireRepellance);
		//Debug.Log ("Before villagers: " + utility);
		//closer to villagers is better
		foreach (GameObject villager in GameObject.FindGameObjectsWithTag("Villager")) {
			utility -= /*Mathf.Log(*/(pos - villager.transform.position).magnitude/*, 2)*/ * VillagerAttraction;
		}
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
			utility -= /*Mathf.Log(*/(pos - player.transform.position).magnitude/*, 2)*/ * VillagerAttraction;
		}
		//Debug.Log("After villagers: " + utility);
		return utility;
	}
}
