﻿using UnityEngine;
using System.Collections.Generic;

//Allows us to use Lists. 
using Pathfinding;

[RequireComponent(typeof(Mesh))] // Guarantee terrain mesh
[RequireComponent(typeof(MeshCollider))] // Guarantee terrain mesh collider
//[RequireComponent(typeof(Forest))] // Guarantee Forest
[RequireComponent(typeof(AstarPath))] // Guarantee AstarPath
public class World : MonoBehaviour
{
	// Access World through World.i
	public static World i = null; 
	public GameObject ScoreMaster;

	// Private members

	void Awake (){
		DontDestroyOnLoad (gameObject);

		// Singleton paradigm
		if (i == null) // Check if instance already exists
			i = this; // if not, set instance to this
		else if (i != this){ // If instance already exists and it's not this
			Debug.LogError("A World already exists, destroying this new one.");
			Destroy (gameObject);
		}

		// We have terrain guaranteed
		//GetComponent<Forest> ().Generate ();


//		gameObject.AddComponent<AstarPath> ();
//
//		GridGraph gg = new GridGraph ();
//		gg.center = Vector3.zero;
//		gg.width = 1024;
//		gg.depth = 1024;
//		gg.nodeSize = 2;
//		gg.maxClimb = 0;
//		gg.maxSlope = 90;
//
//
//		gg.drawGizmos = false;
//		gg.UpdateSizeFromWidthDepth ();
//
//

		// TODO Spawn initial entities

	}

	void Start(){
		AstarPath.active.Scan (); // Scan all the terrain
	}

	public static bool GetGroundPos(Vector2 pos, out RaycastHit hit){
		return GetGroundPos (new Vector3(pos.x, 0, pos.y), out hit);
	}

	public static bool GetGroundPos(Vector3 pos, out RaycastHit hit){
		pos.y = 100000f;
		return World.i.GetComponent<MeshCollider>().Raycast(new Ray (pos, -Vector3.up), out hit, Mathf.Infinity);

	}

    public Vector3 ProjectToTerrain(Vector3 toProject)
    {
        RaycastHit hitInfo;
        if (this.GetComponent<MeshCollider>().Raycast(new Ray(new Vector3(toProject.x, 1000, toProject.z), -transform.up), out hitInfo, Mathf.Infinity))
            return hitInfo.point;
        else
            return toProject;
    }

    public Vector3 ProjectToTerrain(Vector2 toProject)
    {
        RaycastHit hitInfo;
        if (this.GetComponent<MeshCollider>().Raycast(new Ray(new Vector3(toProject.x, 1000, toProject.y), -transform.up), out hitInfo, Mathf.Infinity))
            return hitInfo.point;
        else
            return new Vector3(toProject.x, 0, toProject.y);
    }
 
	//TODO write a getGameObject accepting lambda function
	public static T GetNearest<T>(GameObject[] arr, Vector3 pos){
		return GetNearest<T>(arr, pos, Mathf.Infinity);
	}


	public static T GetNearest<T>(GameObject[] objs, Vector3 pos, float maxDist){
		GameObject nearObj = null;
		float currDist, nearDist = Mathf.Infinity;
		foreach (GameObject currObj in objs) {
			currDist = Vector3.Distance (currObj.transform.position, pos);
			if (currDist < maxDist && currDist < nearDist) {
				nearDist = currDist;
				nearObj = currObj;
			}
		}
		if(nearObj != null)
			return (T) nearObj.GetComponent<T>(); // Can be null if there is no object near enough.
		return default(T); 
	}

	// Wrapper to get the nearest at any distance
	public static T GetNearest<T>(string tag, Vector3 pos){
		return GetNearest<T>(tag, pos, Mathf.Infinity);
	}

	/* Get the GameObject with the specified tag nearest to pos within the maxDist,
	 * Returns null if a GameObject cant be found under the specified conditions */
	public static T GetNearest<T>(string tag, Vector3 pos, float maxDist){
		GameObject nearObj = null;
		float currDist, nearDist = Mathf.Infinity;

		foreach (GameObject currObj in GameObject.FindGameObjectsWithTag(tag) ) {
			currDist = Vector3.Distance (currObj.transform.position, pos);
			if (currDist < maxDist && currDist < nearDist) {
				nearDist = currDist;
				nearObj = currObj;
			}
		}
		if(nearObj != null)
			return (T) nearObj.GetComponent<T>(); // Can be null if there is no object near enough.
		return default(T);
	}

}
