﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gameScore : MonoBehaviour {

	public float matchLength = 30;

	public float villagerCount = 10;

	public float deathThreshold = 5;

	public GameObject winScreen;

	public GameObject loseScreen;

	public GameObject placeLogScreen;

	public GameObject escapeScreen;

	public bool gameStarted;

	public float timePassed;

	public float startingVillagerCount = 10;

	public bool isGameOver;

	public Text counter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(gameStarted == true)
		{

			timePassed += Time.deltaTime;


		}



		checkGameTimer();
	}

	void StartMatch () {

		gameStarted = true;

		placeLogScreen.SetActive(false);

	}

	void villagerDeath () {

		if(isGameOver == false)
		{
		
			villagerCount -= 1;
				
			counter.text = (villagerCount.ToString() + "/10");

			//counter.text

			checkVillagerCount();
		}
	}

	void checkVillagerCount () {

		if(villagerCount <= deathThreshold && isGameOver == false)
		{

			youLose();

		}

	}


	void checkGameTimer () {

		if(timePassed > matchLength && isGameOver == false)
		{

			youWin();

		}

	}
		
	void youWin () {

		winScreen.SetActive(true);

		isGameOver = true;


	}

	void youLose () {

		placeLogScreen.SetActive(false);

		Destroy(escapeScreen);

		loseScreen.SetActive(true);

		isGameOver = true;

	}

	void playerDeath ()

	{
		if(isGameOver == false)
		{

			youLose();

		}


	}
}
