﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class mychar : MonoBehaviour
{
	public CampfireTest campfire;

	// defining the rigidbody
	public Rigidbody rby;

	// defining movement
	public Vector3 moveVector;

	// remembers which direction the character is facing when velocity is 0/when you last moved
	public Vector3 facingVector;

	// we wanna point toward moveVector but REAL SLOWWWW so this will grow up to be moveVector someday but slowwww
	public Vector3 currentDirection;
	public float directionDistance;

	// obv speed, can be changed
	public float speed = 1;

	// defining rotation
	public Vector3 cubeRot;
	public bool isMoving;


	public GameObject lastCamp;

	// controls the 3D model
	public Transform character;
	public Vector3 characterPos;
	public GameObject cam;
	public Animator anim;
	public Vector3 fallingVector;
	public bool canJump;
	public float gravityCounter;
	public float jumpSpeed;
	public bool wolfAttacking;
	public GameObject wolf;
	public GameObject escapeText;
	public float wolfFightTime;
	public float loseWolfThreshold = 9;
	public GameObject scoreMaster;
	public bool rmbPressed = false;
	public bool hasFire = true;

	public ParticleSystem bloodParticles;

	public GameObject fakeFire;

	public float fogMin = .003f;
	public float fogMax = .75f;

	public List<Transform> fireList = new List<Transform>();

	public float shortestDistance;
	public Transform activeFogSpot; 

	//public SmoothFollow camScr;

	// Use this for initialization
	void Start ()
	{
		character.parent = null;

		RenderSettings.fogDensity = fogMin;
	}


	// Update is called once per frame
	// this is where the stuff happens
	void Update ()
	{
		
		FogMachine();

		Vector3 offset = this.transform.forward / 3f;
		float maxDist = 10f;
		GameObject[] camps = GameObject.FindGameObjectsWithTag ("Campfire");
		CampfireTest camp = World.GetNearest<CampfireTest> (camps, this.transform.position + offset, maxDist);
		if (camp != null) {
			lastCamp = camp.gameObject;

			camp.SendMessage("turnOnPopup");

		} else {
			if(lastCamp != null){
				lastCamp.SendMessage("turnOffPopup");

			}
		}

		// If the player presses either E or Q

		bool ePressed = Input.GetKeyDown (KeyCode.E);
		bool qPressed = Input.GetKeyDown (KeyCode.Q);
		if (ePressed || qPressed) {
			// Check for the nearest campfires


			if (camp != null) {


				if(hasFire)
				{
					fakeFire.SetActive(true);
				}

				if (ePressed && (camps.Length > 1 || hasFire)) {
					
					camp.SnuffOut ();
				} else if (qPressed && !hasFire && camp.TakeSubFire ())
					hasFire = true;
				lastCamp.SendMessage("turnOffPopup");
			}

		}

		if ((Input.GetAxis ("Fire1") == 1) && !rmbPressed && hasFire) {
			rmbPressed = true;
			RaycastHit hit;
			if (World.GetGroundPos (transform.position + transform.forward, out hit)) {
				GameObject campySpot = (GameObject)Instantiate(campfire.gameObject, hit.point, Quaternion.identity);
				fireList.Add(campySpot.transform);
				if(activeFogSpot == this.transform)
				{
					activeFogSpot = campySpot.transform;
				}
				hasFire = false;
				fakeFire.SetActive(false);
			}
			GameObject.FindWithTag ("Score").SendMessage ("StartMatch");
		}
		if (Input.GetAxis ("Fire1") == 0)
			rmbPressed = false;

		Rotation ();

		if (wolfAttacking) {
			canJump = false;
			escapeText.SetActive (true);
			fightTheWolf ();
			canJump = true;
			return;
		}

		escapeText.SetActive (false);

		if (Input.GetAxis ("Vertical") == 0 && Input.GetAxis ("Horizontal") == 0) {
			isMoving = false;
			anim.SetBool ("isMoving", false);
		} else {
			isMoving = true;
			anim.SetBool ("isMoving", true);

		}
			
		Movement ();
		Character ();
		Jumping ();

	}

	void Movement ()
	{
		moveVector = transform.right * (speed * Input.GetAxis ("Horizontal")) + transform.forward * (speed * Input.GetAxis ("Vertical")) + fallingVector;
		rby.velocity = moveVector;

		if (isMoving)
			facingVector = moveVector;
	}

	void Rotation ()
	{
		cubeRot = this.transform.localEulerAngles;
		cubeRot.y += Input.GetAxis ("Mouse X") * 3;
		this.transform.localEulerAngles = cubeRot;
	}

	void Character ()
	{

		currentDirection = moveVector;
		currentDirection.y = 0;
		characterPos = this.transform.position;
		characterPos.y -= 0.5f;
		character.position = characterPos;

		if (isMoving) {
			directionDistance = Mathf.Sqrt ((character.rotation.eulerAngles - Quaternion.LookRotation (currentDirection).eulerAngles).sqrMagnitude);
			directionDistance = (directionDistance * 5) / 180;
			directionDistance = Mathf.Clamp (directionDistance, 1, 5);
			character.rotation = Quaternion.Lerp (character.rotation, Quaternion.LookRotation (currentDirection), (Time.deltaTime * 10) / directionDistance);
		}
	}

	void Jumping ()
	{
		if (Input.GetAxis ("Jump") == 1 && canJump == true) {
			jumpSpeed = 15;
			gravityCounter = 0;
			canJump = false;
		}

		fallingVector.y = jumpSpeed - gravityCounter;

		if (canJump == false)
			gravityCounter += Time.deltaTime * 30;

	}

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "Ground") {
			canJump = true;
			gravityCounter = 5;
			jumpSpeed = 0;
		}
	}

	void WolfHit (GameObject wolfObject)
	{
		bloodParticles.Play();
		wolf = wolfObject;
		wolfAttacking = true;

		wolf.SendMessage ("StartStruggle");
	}


	public float escapeTimer = 0;
	public 	float firstPress = 0;
	public int pressedEnough = 10;
	public int howManyPresses = 0;
	public bool escapeSuccessful;

	void fightTheWolf ()
	{
		anim.SetBool ("isAttacked", true);

		//bloodParticles.Play();

		rby.velocity = Vector3.zero;

		wolfFightTime = 0;

		//start timer
		escapeTimer += Time.deltaTime;
		Debug.Log("Fight time started");

		if(escapeTimer >= 10)
		{
			Debug.Log("Fight time stopped. At least, it should have.");

			scoreMaster.SendMessage ("playerDeath");
			anim.SetBool ("isDead", true);
		} else
		{

			if (Input.GetKeyDown (KeyCode.F))
			{
				howManyPresses++;
				Debug.Log("Key presses: " + howManyPresses);
			}

			if(howManyPresses >= 15)
			{
				escapeTimer = 0;
				howManyPresses = 0;
				Debug.Log ("GoAway key pressed. It's F now, not space.");
				wolfAttacking = false;
				bloodParticles.Stop();
				wolf.SendMessage ("GoAway");
				anim.SetBool ("isAttacked", false);
			}
				
		/*if((escapeTimer >= 10) && (howManyPresses <= 15))
		{

			scoreMaster.SendMessage ("playerDeath");
			anim.SetBool ("isDead", true);

		}*/

	}
		
	/*void wolfDamageHandler ()
	{


		float escapeTimer = 0;
		float firstPress = 0;
		int pressedEnough;
		int howManyPresses = 0;

		bool escapeSuccessful;

		//int takingItAway

		pressedEnough = 10;

		escapeTimer += Time.deltaTime;


		if(Input.GetAxis ("EscapeWolf" == 1)
			{

			}





	}*/







	}


	void FogMachine()
	{
		if(fireList != null){
		foreach(Transform fireSpot in fireList)
		{
			
			if(shortestDistance > Mathf.Sqrt((this.transform.position - fireSpot.position).sqrMagnitude))
			{
				activeFogSpot = fireSpot;
			}


		}
		}
		RenderSettings.fogDensity = Mathf.Clamp((Mathf.Sqrt((this.transform.position - activeFogSpot.position).sqrMagnitude)/800) * (fogMax), fogMin, fogMax);

	}


}
