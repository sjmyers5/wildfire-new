﻿using UnityEngine;
using System.Collections;

public class wolfAttack : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter (Collider col) {
	
		if(col.tag == "Wolf" && col.GetComponent<Wolf>().VerifyTarget(this.gameObject.transform))
		{

			this.gameObject.SendMessage("WolfHit", col.gameObject);

			//col.enabled = false;

			col.SendMessage("StartStruggle");

		}

	}


}
