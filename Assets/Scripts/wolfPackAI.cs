﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class wolfPackAI : MonoBehaviour {

	public Transform villager;

	public Transform fire;

	public float distance;

	public float huntThreshold = 400;

	public GameObject wolfObject;

	public GameObject scoreMaster;

	public bool killConfirmed;

	public stalkerUnit tempUnit;


	public List<stalkerUnit> stalkedPeople;

	public int peopleID;

	public Transform playerTracker;

	public float howManyWolves = 5;

	public List<wolfUnit> wolves;

	public wolfUnit defaultWolf;

	[System.Serializable]
	public class wolfUnit
	{

		public GameObject wolf;

		public Transform target;

		public bool isBusy;

		public float timerRun;

	}

	[System.Serializable]
	public class stalkerUnit
	{

		public Transform villager;

		public Transform fire;

		public float distance;

		public bool hasDied;

		public bool mealTaken;

	}





	// Use this for initialization
	void Start () {
	
		for(int i = 0; i < howManyWolves; i++)
		{

			//wolves.Add(defaultWolf);

			wolves[i].wolf = Instantiate(wolfObject, this.transform.position, this.transform.rotation) as GameObject;

		}

	}
	
	// Update is called once per frame
	void Update () {
	
		if(stalkedPeople == null)
		{
			
			return;

		}

		checkHuntable();


		Debug.Log(distance);

	}

	public void watchThese (Transform vill, Transform flame)

	{

		stalkerUnit tempTemp = new stalkerUnit();


		//tempTemp.villager = vill;

		//tempTemp.fire = flame;

		stalkedPeople.Add(tempTemp);

		stalkedPeople[peopleID].villager = vill;

		stalkedPeople[peopleID].fire = flame;

		peopleID++;

		//villager = vill;

		//fire = flame;


	}

	void checkHuntable ()
	{

		for(int i = 0; i < stalkedPeople.Capacity; i++)
		{

			stalkedPeople[i].distance = Mathf.Abs((stalkedPeople[i].fire.position - stalkedPeople[i].villager.position).sqrMagnitude);


			if(stalkedPeople[i].distance > huntThreshold && stalkedPeople[i].hasDied == false)
			{

				stalkedPeople[i].hasDied = true;



				wolvesAssign(i);

				//wolfObject.SetActive(true);

				//wolfObject.SendMessage("killThisPerson", stalkedPeople[i].villager);


			}

			if(Mathf.Abs((stalkedPeople[i].fire.position - playerTracker.position).sqrMagnitude) > huntThreshold)
			{

				killPlayer(i);



			}

		}

		//distance = Mathf.Abs((fire.position - villager.position).sqrMagnitude);

		//if(distance > huntThreshold)
		//{

		//	attack();

		//}

	}

	void killPlayer (int number)
	{

		for(int i = 0; i < wolves.Capacity; i++)
		{

			if(wolves[i].isBusy == false)
			{
				if(stalkedPeople[number].mealTaken == false){
					wolves[i].wolf.SendMessage("killThisPerson", playerTracker);

					wolves[i].isBusy = true;
					wolves[i].timerRun = 0;
					stalkedPeople[number].mealTaken = true;
				}
			}
			else
			{
				wolves[i].timerRun += Time.deltaTime;

				if(wolves[i].timerRun > 9)
				{
					wolves[i].isBusy = false;
					wolves[i].wolf.SendMessage("GoAway");

				}

			}


		}


	}

	void wolvesAssign (int number)
	{

		for(int i = 0; i < wolves.Capacity; i++)
		{

			if(wolves[i].isBusy == false && stalkedPeople[number].mealTaken == false)
			{

				wolves[i].wolf.SendMessage("killThisPerson", stalkedPeople[number].villager);

				wolves[i].isBusy = true;
				stalkedPeople[number].mealTaken = true;

			}


		}


	}

	void attack ()
	{

		if(killConfirmed == false)
		{

			wolfObject.SetActive(true);

			wolfObject.SendMessage("killThisPerson", villager);

			scoreMaster.SendMessage("villagerDeath");

			killConfirmed = true;
		}
	}


}
